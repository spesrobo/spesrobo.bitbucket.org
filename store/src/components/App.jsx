import React from 'react';

export default class extends React.Component {
  getThumbImageUrl() {
    return 'images/' + this.props.id + '.png';
  }

  getAppUrl() {
    return '../apps/' + this.props.id + '/index.html';
  }

	render() {
		return (
				<div className="col-md-3">
					<a className="thumbnail application" href={this.getAppUrl()}>
						<div className="image-holder">
							<img src={this.getThumbImageUrl()} style={{width: '100%'}} />
						</div>
						<div className="caption">
							<h3>{ this.props.title }</h3>
							<p>{ this.props.description }</p>
						</div>
					</a>
				</div>
      );
  }
}
