// https://flatuicolors.com/

import $ from 'jquery';
global.jQuery = $; require ('bootstrap');
import './app.css';
import Menu from './Menu.jsx';
import Workspace from './Workspace.jsx';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
	<div>
		<Menu />
		<Workspace />
	</div>,
  document.getElementById('application')
);
