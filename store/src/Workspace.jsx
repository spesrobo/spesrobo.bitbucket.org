import React from 'react';
import App from './components/App.jsx';

export default class extends React.Component {
	constructor(params) {
		super(params);

		this.apps = [
			{
				title: 'Online IDE',
				id: 'com.spesrobo.ide',
				description: 'Make first steps with SpesRobo, use our custom IDE',
			},
			{
				title: 'Arm Controller',
				id: 'com.spesrobo.armcontroller',
				description: 'Control SpesRobo\'s robotic arm and watch 3D simulation',
			},
			{
				title: 'Move Controller',
				id: 'com.spesrobo.movingcontroller',
				description: 'Use this application to move around and watch what\'s happening',
			},
			{
				title: 'Object Tracker',
				id: 'com.spesrobo.objecttracker',
				description: 'Use power of OpenCV embeded into our SpesRobo application',
			},
			{
				title: 'Virtual Reality',
				id: 'com.spesrobo.vr',
				description: 'Put your phone on the head and explore the world from SpesRobo point of view'
			},
			{
				title: 'Metal Detector',
				id: 'com.spesrobo.metaldetector',
				description: 'Let robot to find metal around'
			},
		];
	}


	render() {
		var apps = [];
		for (var i = 0; i < this.apps.length; i++) {
			apps.push(
				<App key={'app' + this.apps[i].id} title={this.apps[i].title} id={this.apps[i].id} description={this.apps[i].description}></App>
			);
		}

		return (
			<div className="container">

				<div className="row">
					<div className="col-md-12">
						<h4>All applications</h4>
					</div>
				</div>

				<div className="row">
					{apps}
				</div>
			</div>
		);
	}
}
