# SpesRobo web application collection

## Getting started
- Install Node.js, npm and git,
- Install webpack globally ```npm install webpack -g```
- Install webpack globally ```npm install http-server -g```
- Clone this repo ```git clone https://bitbucket.org/spesrobo/web-controller.git```
- Change directory ```cd web-controller```
- Install packages ```npm install```
- Change directory ```cd controller``` or ```cd ide``` or ```cd store```
- Run webpack ```webpack --watch```
- In another console run ```http-server``` and open 127.0.0.1:8080 with you browser 