import React from 'react';
import './circle.css';

export default class extends React.Component {
	componentWillReceiveProps() {
		this.refs.circle.style.transform = 'rotate(' + this.props.angle + 'deg)';
	}

	render() {
		return (
			<div className="circle-holder">
				<div ref="circle" className="circle">
					<div className="circle-dot">
					</div>
				</div>
			</div>
		);
	}
}
