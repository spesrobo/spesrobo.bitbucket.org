import React from 'react';
import THREE from 'three';
var OrbitControls = require('three-orbit-controls')(THREE)

export default class extends React.Component {
  constructor(params) {
    super(params);

    this.HEIGHT = this.props.height;
    this.WIDTH = this.props.width;

    this.sceneRender = this.sceneRender.bind(this);
  }

  componentDidMount() {
    // Init renderer
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(this.WIDTH, this.HEIGHT);
    this.renderer.setClearColor(0xeeeeee, 1.0)
    this.refs.container.appendChild(this.renderer.domElement);

    this.scene = new THREE.Scene();
    this.scene.add(new THREE.AxisHelper(200)); // The X axis is red. The Y axis is green. The Z axis is blue.
    this.camera = new THREE.PerspectiveCamera(50, this.WIDTH / this.HEIGHT, 1, 1000);
    this.camera.position.set(250, 0, -150);
    this.camera.rotation.x = 20;


    new OrbitControls(this.camera, this.refs.container);


    // Make light
    var pointLight = new THREE.PointLight( 0xFFFFFF );
    pointLight.position.x = 300;
    pointLight.position.y = 500;
    pointLight.position.z = -100;
    this.scene.add(pointLight);

    this.makeArm();
    this.shouldComponentUpdate();

    requestAnimationFrame(this.sceneRender);
  }



  makeArm() {
    this.arm = {
      cylinders: []
    };

    var CYLINDER_RADIOUS = 3;
    var CYLINDER_COLOR = 0x888888;

    var crank = new THREE.Mesh(
      new THREE.SphereGeometry(5),
      new THREE.MeshLambertMaterial({ color: 0xCC0000 })
    );

    // First crank
    this.arm.cylinders[0] = new THREE.Object3D();
    var firstCylinder = new THREE.Mesh(
      new THREE.CylinderGeometry(CYLINDER_RADIOUS, CYLINDER_RADIOUS, 110),
      new THREE.MeshLambertMaterial({ color: CYLINDER_COLOR })
    );
    firstCylinder.position.y = 55; //firstCylinder.height / 2;
    this.arm.cylinders[0].add(firstCylinder);
    this.arm.cylinders[0].add(crank.clone());
    this.scene.add(this.arm.cylinders[0]);

    // Second crank
    this.arm.cylinders[1] = new THREE.Object3D();
    var secondCylinder = new THREE.Mesh(
      new THREE.CylinderGeometry(CYLINDER_RADIOUS, CYLINDER_RADIOUS, 110),
      new THREE.MeshLambertMaterial({ color: CYLINDER_COLOR })
    );
    secondCylinder.position.y = 55;
    this.arm.cylinders[1].add(secondCylinder);
    this.arm.cylinders[1].add(crank.clone());
    this.scene.add(this.arm.cylinders[1]);


    // Third crank
    this.arm.cylinders[2] = new THREE.Object3D();
    var thirdCylinder = new THREE.Mesh(
      new THREE.CylinderGeometry(CYLINDER_RADIOUS, CYLINDER_RADIOUS, 50),
      new THREE.MeshLambertMaterial({ color: CYLINDER_COLOR })
    );
    var gripper = new THREE.Mesh(
      new THREE.SphereGeometry(10),
      new THREE.MeshLambertMaterial({ color: 0xCC0000 })
    );
    gripper.position.y = 50;
    thirdCylinder.position.y = 25;
    this.arm.cylinders[2].add(thirdCylinder);
    this.arm.cylinders[2].add(crank.clone());
    this.arm.cylinders[2].add(gripper);
    this.scene.add(this.arm.cylinders[2]);
  }

  shouldComponentUpdate() {
    // Calculate angles
    var firstAngle = (this.props.first * Math.PI) / 180 - Math.PI / 2; // Convert to degrees
    var secondAngle = (this.props.second * Math.PI) / 180 - Math.PI + firstAngle;
    var thirdAngle = (this.props.third * Math.PI) / 180 - Math.PI + secondAngle;

    console.log(this.props);

    // Set cylinders angles
    this.arm.cylinders[0].rotation.x = firstAngle;
    this.arm.cylinders[1].rotation.x = secondAngle;
    this.arm.cylinders[2].rotation.x = thirdAngle;

    // Set X axis
    this.arm.cylinders[0].position.x = this.props.x;
    this.arm.cylinders[1].position.x = this.props.x;
    this.arm.cylinders[2].position.x = this.props.x;

    return true;
  }

  sceneRender() {
    // Set second cylinder start position
    this.arm.cylinders[1].position.z = Math.cos(this.arm.cylinders[0].rotation.x - Math.PI / 2) * 110;
    this.arm.cylinders[1].position.y = - Math.sin(this.arm.cylinders[0].rotation.x - Math.PI / 2) * 110;

    // Set third cylinder start position
    this.arm.cylinders[2].position.z = Math.cos(this.arm.cylinders[1].rotation.x - Math.PI / 2) * 110 + this.arm.cylinders[1].position.z;
    this.arm.cylinders[2].position.y = - Math.sin(this.arm.cylinders[1].rotation.x - Math.PI / 2) * 110 + this.arm.cylinders[1].position.y;


    this.renderer.render(this.scene, this.camera);
    requestAnimationFrame(this.sceneRender);
  }

  render() {
    return <div ref="container" style={{width: this.WIDTH, height: this.HEIGHT, backgroundColor: '#000'}}></div>;
  }
}
