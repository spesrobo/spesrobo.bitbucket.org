import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import 'bootstrap-slider';
import Arm3D from './components/Arm3D.jsx';
import Circle from './components/Circle.jsx';

export default class extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			angles: []
		};
		this.sliders = [];
		this.sliderConfigs = [
			{min: 30, max: 100, title: 'First crank', value: 60, command: 'af'},
			{min: 65, max: 130, title: 'Second crank', value: 80, command: 'as'},
			{min: 120, max: 250, title: 'Third crank', value: 100, command: 'at'},
			{min: -100, max: 100, title: 'Slider', value: 0, command: 'ax'},
			{min: 0, max: 100, title: 'Gripper', value: 50, command: 'ag'},
			{min: 0, max: 100, title: 'Basket', value: 50, command: 'ba'},
		];
	}

	componentWillMount() {
		for (let i = 0; i < this.sliderConfigs.length; i++) {
			this.state.angles[i] = this.sliderConfigs[i].value;
		}
	}

	componentDidMount() {
		var that = this;

		// Init SpesRobo UI
		global.spesrobo.makeUI();

		// Init Sliders
		for (let i = 0; i < this.sliderConfigs.length; i++) {
			this.sliders[i] = $(this.refs['crankSlider' + i]).slider(this.sliderConfigs[i]);
			that.state.angles[i] = this.sliderConfigs[i].value;

			this.sliders[i].change(function(object) {
				// Direct communication with Arduino
				global.spesrobo.hardware.setCommand(that.sliderConfigs[i].command + object.value.newValue);
				that.state.angles[i] = object.value.newValue;
				that.setState(that.state);
			});
		}
		this.setState(this.state);
	}

	render() {
		var that = this;

		return (
			<div>


				<div className="row">
					<div className="col-md-6 col-md-offset-3 hidden-sm hidden-xs">
						<Arm3D
							width={500}
							height={250}
							first={this.state.angles[0]}
							second={this.state.angles[1]}
							third={this.state.angles[2]}
							x={this.state.angles[3]}>
						</Arm3D>
					</div>
				</div>

				<div className="row">
					{ this.sliderConfigs.map(function(sliderConfig, i) {
						return (
							<div className="col-md-3 col-sm-6" key={'circle' + i}>
								<div className="thumbnail">
									<div className="caption">
										<Circle angle={that.state.angles[i]}></Circle>
										<h3>{ sliderConfig.title }</h3>
										<input type="text" ref={'crankSlider' + i} />
									</div>
								</div>
							</div>
						)
					})}
				</div>
			</div>
		);
	}
}
