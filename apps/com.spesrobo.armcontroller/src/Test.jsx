import React from 'react';

export default class extends React.Component {
  constructor(props) {
    super(props);
    var that = this;

    this.handleClick = this.handleClick.bind(this); // To use this in handleClick()
    this.state = {
      rand: this.getRand(),
      onSampleEventOut: '',
      sampleMethodReturn: ''
    };

    // SpesRobo API
    global.spesrobo.onready(function() {
      var tt = that;
      that.test = new global.spesrobo.Test(global.spesrobo); // Init Test class
      that.test.onSampleEvent(function(data) { // Listen for event
        tt.state.onSampleEventOut = data;
        tt.setState(tt.state);
      });
    });


  }

  getRand() {
    return Math.floor(Math.random() * 10000);
  }

  handleClick(e) {
    e.preventDefault();

    var that = this;

	// SpesRobo API
    this.test.sampleMethod(this.state.rand, function(data) {
      that.state.sampleMethodReturn = data;
      that.setState(that.state);
    });


	// SpesRobo API
	// global.spesrobo.hardware.setArmCoordinates(40, 30, 20);


    this.state.rand = this.getRand();
    this.setState(this.state);
  }

  render() {
		return (
      <div className="row">
        <div className="col-md-12">
          <h2>Test</h2>
          <button className="btn btn-primary" onClick={this.handleClick}>Test.sampleMethod({this.state.rand})</button>

          <div className="row">
            <div className="col-md-12">Event result: <b>{this.state.onSampleEventOut}</b></div>
          </div>
          <div className="row">
            <div className="col-md-12">Method return: <b>{this.state.sampleMethodReturn}</b></div>
          </div>
        </div>
      </div>
    );
  }
}
