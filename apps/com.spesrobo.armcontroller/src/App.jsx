import $ from 'jquery';
global.jQuery = $; require ('bootstrap');
import './app.css';
import Workspace from './Workspace.jsx';
import React from 'react';
import ReactDOM from 'react-dom';
import SpesRobo from '../../../lib/SpesRobo.jsx';

// Init SpesRobo
global.spesrobo = new SpesRobo();

ReactDOM.render(
	<div>
		<Workspace />
	</div>,
  document.getElementById('application')
);
