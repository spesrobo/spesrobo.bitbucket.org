const DEFAULT_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAADGpJREFUeJzt3XuMXGUZx/Fvt9vthW6x0EKVUASpIBeNCAqIcjFe8Q/xRsRLUPlDopIgGqNoMNFoogEEjFEUw0WQiyRGQiwEqIoiKCKgWEgLolyWS4F2e9tud2f94zmTTpft7pl53nOec2Z+n+TJzBLmPc97ep4513lfEBERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERGprlnRCRSgD5ibxUDL65zsdaDl7zlA/6TXOcDslv82OWbniL5dxKyW19Zo/W9M875pYoq/W6Oxi7/Hs/etMT5NjLW8bs9em++bMZpF8/22STHS8ro1+39qo+wC6QMWAbtnsQgYzGIhsFvL64Is5mexAJiXvW99bUazKGaX1hvpRAMrlGZszmJTy+tGYBjYkMV64EVgHfAC8Hz2d+FSFsirgMOA/YHlwCuBvYClwBJgMVYUfQmXKb1rGzAEPAU8CqwB7gfuoqTiyetgLLnJu3mFIiJGga+RSIo9yEXAWQnaEUllG3bUssnbUIrDnbcnaEMkpbnA8Ska8hZIP3BIikREEjsqRSPeAlmOXTIVqZpDUzTiLZD9UyQhUoAVKRrxFsi+KZIQKcABKRrxFsg+KZIQKcAgdiPaxVsgy7wJiBTI/QXuLZCl3gRECrS3twFvgSzxJiBSoL28DXgLZE9vAiIFch/heAtksTcBkQK5v8BVINLN3Nunp0D6sEtpIlUVWiAL6c5fJEr3eIW3AU+BaO8hVRd6o1AFIlWnAhGZhnsb9RTIbt6FixTMvY2qQKSbLfA2oAKRbha6B3FXp0jBBnCOk6YCkW433/NhT4G4FixSEtd22h+14MQawOPAI8AT2HhIW7AhSRdiP+w6CHgNGmSiU+uw9bsWGw50E/YkxSB2x3oFto7dd68TCyuQeZ4FJ/AkcD1wO3AnNp7rTOYCRwMnAR8i0cgXXWoEuAlYCdyBfQHlcRBwInAy8B5821gKYdvpDyh/WMkG8FvgHaQZ9O4I4OfYcJXRQ2ZWJdYAn8PGUfZaCpyDfZlF9efwBP3oyMU5E0wVvwNeX1Bf9sUKZfK0Ab0UTwAfp5jR8QeAM7HDtLL79aYC+pPLpTkT9MYQdjhUhmOAf5XQpyrFOHA+dq5WtD2By0ru37El9GtKl+dIzhu3keCH922aj+1NojfcMuJZ4J1pVltbTsXm/yijjyeW1KeXuSZngp3Gj4mdS+QL2Ldr9EZcVKwmduC/Q7DDuqL7+a6yOjTZDTkT7CS+XWI/pnMqNrVY9MacOu6lGgNu7ItdOi6yryeX1ptJfpMzwXbjwjI7kcOnid+gU8ZqqjVc034Ue5XrlPK6srObcybYTtxINX/Gex7xG3aKeB4bkb9qDsfmJyyizx8tsR87uTVngnljLQl+AVaQPtL3t+xoAO9NvWISKmpPfVqZnWi1KmeCef/xji43/bbtDbxE/IbeaVySfpUkV8R57SdL7UGLP+VMME9cWnLunfo88Rt6JzFEdffOrfbBHhlK2ffTy+xAq7tzJJcnNlKNKyp59FHPG4mfLWJlFORc0vb9DE8ynvsMqR5C+wk2OXwdNIDvRSfRpv8BV0Yn0YZLgPUJ2wv7wVSKZ3bGgAsStFOma7GNri5+iN3LqYth4KcJ23PdbI4ukFux4+M6GQd+GZ1ETmPUJ9dWVyRsq9Z7kDr+4wFcFZ1ATrdi9z7qZjXw90Rt1XYPMoH9A9bRw9TjMGtldAIOtyRqJ6xAvA8SPkh9Ts6nckd0Ajmsik7AIdX6dT2ZEVkg9zo/H63q+W8FHopOwiHV+q1tgTzi/Hy0que/BjuMrasN2O9VvMIKxPtQ4WPOz0d7NDqBGdR9/UIF1nFkgQw7Px9tQ3QCM6j7+oU067i2h1h5humpsqrnX/X88gjvQ+RPWosYPaNMVc+/6vnlEd6HyEOsMkbRKFLVJxCq+/qFNOvYdaEicg+yR+CyU6h6/lXPL4/wPkQWyIrAZafw2ugEZlD39Qtp1nFt9yAHBy47hYOiE5jB/tR7oO5lpPmRV1iBeG9CHeP8fLS3Ricwg37gqOgkHFKt34bnw5F7kFdj33J11AccH51EDidFJ+CQakTE2hYIwPuDl9+pY4HF0UnkUNf1O4t0A76FFYhrwZlPJWgjQl3yfjPVP1eaytuwI4wUwgokxYNwR1LclAZFGSRwMLIO1GnAhqaUOY8nbKstj5Jm1IlflZ2401eJH6WknRimHoeDTfuRdjxkV7FF70HAvo1fl6itog0CX4pOok2DwNnRSbThXNJO2xa2B0k5KvdtJefeqQuI3yN0EluBAwpYH6kdSfopJ8JGVlydM8HKdySnN1LvqRBWUs2BwZvmYL8iTN3vj5XZiVapRxgcprqPRwyy4xd6dY4vp14xCZ1PMX3+SJmdaPVAzgTbiQeo3lOyfdi0DNEbd4rYDpyQdO2k8WGK6/MHSuzHTu7LmWC7cTs2n3lV/Ij4DTtlrAfekHQN+ZyAzcleVH/Dbpb+LWeCncTNwILyujKlWRS324+OZ7FzqmgnYj+rLbKv7y6tN5P8JWeCncZdxE0VNhcb9TF6Qy4yhomZ4bbpVIrdczQj7Hm0O3Mm6IknsccOyrQC+EeC3OsQ49j0cmU+kzcAXJy4H9PFceV06+VW5UzQG2PYxJ5Fn7z3A+eQfgKXOsSfKeeRn+OwETXL7NtbSujXlMqes+8p4EzSn8DPAj4I/LPk/kyO7ZRzyLGrGMPmpi9iks+DsUPWRkC/ws61ipjlNm+hfBP/b0mWAl8kbsao3wNfwe4e78GOm3iLgcOy3FZS/s3JUWzCnZPw3Vjsxx5Zv5H0d8fbiUMdfXApap70vNEA7gG+i51sLpsh38XYr9S+jj3aMhqU97W0dzhzAPCzoHyfAC4DPoH9Pny6Z6QGgEOAz2B7i+cC8p0qDpxpBU/H8w1xA3aDp0o2YCf2G4FNwHzs3GUZsFdgXmB7vjPofEqCI7ANL/LBzu3Af7F7KRux7WcR9uWznAqMYzWF5Vihl+5q4r8d6hL3MPMeLo95aL23G0s7WtMZz+W9Uc+Ce8gfsbvFzyRoawQ73KnbvI6Rtnk+rAIp1n3Yow5bE7Y5gV2Ovihhm91sxPNhT4G4KrMHrANOobgBmM8Griuo7W7RwPlF7ikQV2X2gNMpdh7DCeyKUZ1nkSqaexvVHqQYV2L3iYq2BbuSuKWEZdWR+9BWe5D01mPnCGV5GPhGicurk9ACSXni2U2+g51/lOki7FKy7GyztwFPgWi3/nLPYM8zla0BnIWdl8gO7m1Ue5C0LiRuvfwVuCZo2VUVWiDag+xsM3BpcA7fInAcqAoKPcRyL7zLXI+doEdaiz2vJWaTtwEVSDpXRCeQ+T46F2kKLRD3wrvIEPbMVRX8G7glOomKUIFURPPHY1URcSWtityP+XgKZNi78C5yQ3QCk9yM/f6k123wNuApkKIewqubIWywuyppoJN1CC6QzeiSItj8JlVcD9dGJ1ABoQUygQ6zoLrf1Pdjkxz1spe8DXgHDHMnUHMPYYPMVdVV0QkEU4EEq+reo+kKqnV1rWwvehvwFsgL3gRqbAIbQKHKHqc692ciuJ+q9hbI894EauwPBA0n06bLoxMIMkYF9iDPeROosboc3/+a3nws6AUSHF56C2TIm0BNjWAbXh1swob/7DVJtk1vgfTq3dqbqNcl7qo8SFmmp1M04i2QIkftqLK6HF41rcKGDO0lSbZNb4H8J0USNbOOzsfXjTJB/Yra67EUjXgL5GnqdaiRwnXYIM5102uHWY+kaMRbIBPY1M29pOo3B3dlLTaTVK94MEUjKeamuy1BG3WxBrg7OgmHy6MTKMnjWVTCEuAX2ExNW4gf7r7IOC/ROouyiO79N2pgV1VvAd6XaoV5JtDZVXtL2DFhzR5Z7N4Sg8DCLHbD5kNvxnxsDox5TD+bUZQDqf8TslcDp0UnMckoNlzSCFbAzdicRXNCpGHsEfYN2HOAL2IXTZ7B7nskHw439UY4gT1+kuIRlH5sws65WME03w9MijlZtL6fk32++doas6d4P3ua6Mteh6h/cYBNwTyGfeOOT/E6VYxl0fp+DLtY0fp+NHttfT/aEtsmxUj22ii0xyIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiXer/bvxc6iLml1YAAAAASUVORK5CYII='
const CAMERA_INTERVAL = 100;

var spesrobo = null;
var camera = null;

// Images
var leftNode = null,
  rightNode = null;
var leftReportNode = null,
  rightReportNode = null;

function setReport(alpha, gama) {
  var text = 'A: ' + alpha + '<br />' + 'G: ' + gama;

  if (alpha > 45 || alpha < -45) {
    text += "<br /><span style='font-size: 16px'>Keep A in range (-45, 45)</span>";
  }

  leftReportNode.innerHTML = text;
  rightReportNode.innerHTML = text;
}

function requestFullscreen(i) {
  if (i.requestFullscreen) {
    i.requestFullscreen();
  } else if (i.webkitRequestFullscreen) {
    i.webkitRequestFullscreen();
  } else if (i.mozRequestFullScreen) {
    i.mozRequestFullScreen();
  } else if (i.msRequestFullscreen) {
    i.msRequestFullscreen();
  }
}

function onGyroscopeUpdate(event) {
  var alpha = Math.round(event.alpha);
  var gamma = Math.round(event.gamma);
  var normalizedAlpha = Math.round(-(alpha - 180) * 0.5);
  var normalizedGamma = ((gamma > 0) ? 90 : -90) - gamma;

  console.debug('A: ' + normalizedAlpha);

  setReport(normalizedAlpha, normalizedGamma);
  spesrobo.hardware.setKinectAngleHorizontal(normalizedAlpha);
  spesrobo.hardware.setKinectAngleVertical(normalizedGamma);
}

function onSpesRoboReady() {
  console.debug('SpesRobo Communication is ready');

  // Camera
  camera = new spesrobo.Plugins.KinectCamera(spesrobo);
  loadFrame();
}

function loadFrame() {
  camera.getFrame(function(data) {
    rightNode.setAttribute('src', data);
    leftNode.setAttribute('src', data);
  });

  setTimeout(loadFrame, CAMERA_INTERVAL);
}

function initial() {
  if (window.DeviceMotionEvent == undefined) {
    alert('Browser is not supported');
  } else {
    window.onload = function() {
      // Nodes
      leftNode = document.querySelector('#left');
      rightNode = document.querySelector('#right');
      rightReportNode = document.querySelector('#right-report');
      leftReportNode = document.querySelector('#left-report');

      // Images
      rightNode.setAttribute('src', DEFAULT_IMAGE);
      leftNode.setAttribute('src', DEFAULT_IMAGE);

      // Listen for gyroscope
      window.ondeviceorientation = onGyroscopeUpdate;

      // Initial SpesRobo
      spesrobo = new SpesRobo.default();
      spesrobo.makeUI();
      spesrobo.onready(onSpesRoboReady);

      // Fullscreen
      leftNode.onclick = function() {
        requestFullscreen(document.querySelector('#vr'));
      }
      rightNode.onclick = function() {
        requestFullscreen(document.querySelector('#vr'));
      }
    };
  }
}

initial();
