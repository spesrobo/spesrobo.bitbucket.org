import $ from 'jquery';
global.jQuery = $; require ('bootstrap');
import './app.css';;
import Workspace from './Workspace.jsx';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
	<div>
		<h1>SpesRobo IDE</h1>
		<div className="container">
			<Workspace />
		</div>
	</div>,
  document.getElementById('application')
);
