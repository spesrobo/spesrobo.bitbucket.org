export default class {
  static get(name) {
    var codes = {};

    // Basic move
    codes.basicMove = `sr.hardware.moveSpeed(200);

                        setTimeout(function() {
                            sr.hardware.moveForward(100);
                        }, 50);

                        setTimeout(function() {
                            sr.hardware.moveBack(100);
                        }, 2000);`;

    // Basic Camera
    codes.basicCamera = `var camera = new sr.Plugins.FrontCamera(sr);
                        camera.getFrame(function(data) {
                          debug.message(Math.random());
                          debug.image('IMAGE_ID', data);
                        });`;

    // liveCamera
    codes.liveCamera = `var camera = new sr.Plugins.FrontCamera(sr);
                        var n = 0;

                        function show() {
                            setTimeout(function() {
                                camera.getFrame(function(data) {
                                   debug.message(Math.random());
                                   debug.image('IMAGE_ID', data);
                                });

                                if (n < 100) {
                                    n++;
                                    show();
                                }
                            }, 50);
                        }
                        show();`;

    return codes[name].replace(/ {24}/g, '');
  }
}
