import AppDispatcher from '../dispatcher/AppDispatcher.jsx';
import AppConstants from '../constants/AppConstants.jsx';

export default class DebugConsoleActions {
  static addMessage(data) {
    AppDispatcher.handleViewAction({
      actionType: AppConstants.ADD_DEBUG_MESSAGE,
      data: data
    })
  }

  static addImage(id, image) {
    AppDispatcher.handleViewAction({
      actionType: AppConstants.ADD_DEBUG_IMAGE,
      id: id,
      image: image
    })
  }
}
