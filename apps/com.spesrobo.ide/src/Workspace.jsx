import ace from 'brace';
import 'brace/worker/javascript';
import 'brace/ext/language_tools';
import 'brace/mode/javascript';
import 'brace/theme/chrome';

import React from 'react';
import Autocomplete from './Autocomplete.jsx';
import CodeExamples from './CodeExamples.jsx';
import SpesRobo from '../../../lib/SpesRobo.jsx';

import DebugConsole from './components/DebugConsole.jsx';
import DebugConsoleActions from './actions/DebugConsoleActions.jsx';

// Init SpesRobo
global.spesrobo = new SpesRobo();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      image: null,

      debug: {
        messages: [],
        images: []
      }
    };

    this.onBtnExecuteClick = this.onBtnExecuteClick.bind(this);
    this.onCodeExampleChange = this.onCodeExampleChange.bind(this);
  }

  componentDidMount() {
    global.spesrobo.makeUI();

    this.initACE();
  }

 onBtnExecuteClick(e) {
   e.preventDefault();

   // Import global objects debug & sr
   var debug = {
     message: DebugConsoleActions.addMessage,
     image: DebugConsoleActions.addImage
   };
   var sr = global.spesrobo;

   // Run code
   var code = this.editor.getValue();
   eval(code);
 }

onCodeExampleChange(e) {
  this.editor.getSession()
    .setValue(CodeExamples.get(this.refs.selectCodeExample.value));
}

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-9">
            <div id="editor" ref="editor"></div>
          </div>
          <DebugConsole></DebugConsole>
        </div>

        <div className="row control-menu">
          <div className="col-md-3">
            <div className="btn-group" onClick={this.onBtnExecuteClick}>
              <button type="button" className="btn btn-primary">Run the code</button>
              <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                <span className="caret"></span>
              </button>
              <ul className="dropdown-menu">
                <li><a href="#">Debug the code</a></li>
              </ul>
            </div>
          </div>

          <div className="col-md-2">
            Choose an example:
            <select onChange={this.onCodeExampleChange} ref="selectCodeExample">
              <option value="basicMove">Basic Move</option>
              <option value="basicCamera" selected="selected">Basic Camera</option>
              <option value="liveCamera">Live Camera</option>
            </select>
          </div>

        </div>
      </div>
    );
  }

  initACE() {
    var that = this;

    this.editor = ace.edit("editor");
    this.editor.getSession()
      .setMode('ace/mode/javascript');
    this.editor.setTheme('ace/theme/chrome');

    this.editor.setOptions({
      enableBasicAutocompletion: true,
      enableSnippets: true,
      enableLiveAutocompletion: true
    });

    var autocomplete = new Autocomplete(this.editor);

    this.editor.getSession()
      .setValue(CodeExamples.get('basicCamera'));

    var staticWordCompleter = {
      getCompletions: function(editor, session, pos, prefix, callback) {
        var words = autocomplete.getKeywords();
        callback(null, words);
      }
    }

    this.editor.completers = [staticWordCompleter];
  }
}
