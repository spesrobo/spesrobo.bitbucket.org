import AppDispatcher from '../dispatcher/AppDispatcher.jsx';
import AppConstants from '../constants/AppConstants.jsx';
import Store from './Store.jsx';

var CHANGE_EVENT = 'change';

var messages = [];
var images = [];

class DebugConsoleStore extends Store {
  getState() {
    return {
      images: images,
      messages: messages
    };
  }

  addChangeListener(callback) {
    this.on(CHANGE_EVENT, callback);
  }
}

var debugConsoleStore = new DebugConsoleStore();
debugConsoleStore.dispatchToken = AppDispatcher
  .register(function(payload) {
    var action = payload.action;

    switch (action.actionType) {
    case AppConstants.ADD_DEBUG_MESSAGE :
      messages.push(action.data);
      debugConsoleStore.emitChange();
      break;

    case AppConstants.ADD_DEBUG_IMAGE :
      images[action.id] = action.image;
      debugConsoleStore.emitChange();
      break;
    }

    return true;
  });

export default debugConsoleStore;
