import React from 'react';
import DebugConsoleActions from '../actions/DebugConsoleActions.jsx';
import DebugConsoleStore from '../stores/DebugConsoleStore.jsx';
import $ from 'jquery';

export default class extends React.Component {
  constructor(params) {
    super(params);
    this.state = DebugConsoleStore.getState();
  }

  componentDidMount() {
    var that = this;
    DebugConsoleStore
      .addChangeListener(function() {
        that.setState(DebugConsoleStore.getState());
      });
  }

  componentDidUpdate() {
    this.refs.messages.scrollTop = 99999;
  }

  onImageClick(id) {
    this.state = DebugConsoleStore.getState();
    this.state.activeImage = id;
    this.setState(this.state);
    $(this.refs.imageModal).modal('toggle');
  }

  render() {
    // Messages
    var messages = [];
    this
      .state
      .messages
      .forEach(function(value) {
        messages.push(<div className="list-group-item">
          {value}
        </div>);
      });

    // Images
    var images = [];
    for (var id in this.state.images) {
      images.push(<div className="thumbnail" onClick={this
        .onImageClick
        .bind(this, id)}>
        <img className="image" src={this.state.images[id]}/>
        <div className="caption">
          <h3>{id}</h3>
        </div>
      </div>);
    }

    // Go out
    return (
      <div className="col-md-3 debugger">
        {this.renderImageModal()}

        Debug messages
        <div className="list-group messages" ref="messages">
          {messages}
        </div>

        Debug images
        <div className="images" ref="images">
          {images}
        </div>
      </div>
    );
  }

  renderImageModal() {
    return (
      <div className="modal fade" id="imageModal" ref="imageModal" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 className="modal-title">{this.state.activeImage}</h4>
            </div>
            <div className="modal-body">
              <img width="100%" src={DebugConsoleStore.getState().images[this.state.activeImage]} />
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
