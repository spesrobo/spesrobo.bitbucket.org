export default class Autocomplete {
  constructor(_editor) {
    this.editor = _editor;
    this.initKeywordList();
  }

  getKeywords() {
    let filteredKeywords = [];
    let previousKeyword = this.findPreviousKeyword();

    this.keywords.forEach(function(value) {
      if (value.after == previousKeyword) {
        filteredKeywords.push(value);
      }
    });

    return filteredKeywords;
  }

  findPreviousKeyword() {
    var editor = this.editor;
    var range = {
        start: editor.selection
        .getCursor(),
      end: editor.selection
        .getCursor()
    };

    // Find end
    range.start.column--;
    while (true) {
      let text = editor.session.getTextRange(range);
      if (text[text.length - 1] == '.' || range.end.column <= 1) {
        range.end.column--;
        range.start.column--;
        break;
      }
      range.end.column--;
      range.start.column--;
    }

    // Find begin
    range.start.column = range.start.column - 1;
    while (true) {
      let text = editor.session.getTextRange(range);
      if (' .='.indexOf(text[0]) >= 0 || range.start.column <= 0) {
        if (range.start.column > 0) {
          range.start.column++;
        }
        break;
      }
      range.start.column--;
    }
    return editor.session.getTextRange(range);
    console.log(editor.session.getTextRange(range));
  }

  initKeywordList() {
    this.keywords = [
      {
        caption: 'sr',
        value: 'sr',
        meta: 'object',
        after: ''
      },
      {
        caption: 'var',
        value: 'var',
        meta: 'keyword',
        after: ''
      },
      {
        caption: 'new',
        value: 'new',
        meta: 'keyword',
        after: ''
      }, {
        caption: 'hardware',
        value: 'hardware',
        meta: 'object',
        after: 'sr'
      }, {
        caption: 'setArmCoordinates',
        value: 'setArmCoordinates(x, y, z);',
        meta: 'method',
        after: 'hardware'
      },
      {
       caption: 'Plugins',
       value: 'Plugins',
       meta: 'class',
       after: 'sr'
     },
     {
      caption: 'FrontCamera',
      value: 'FrontCamera(sr);',
      meta: 'class',
      after: 'Plugins'
    },
      {
        caption: 'debug',
        value: 'debug',
        meta: 'object',
        after: ''
      }, {
        caption: 'image',
        value: 'image(id, buffer);',
        meta: 'method',
        after: 'debug'
      }, {
        caption: 'message',
        value: 'message(data);',
        meta: 'method',
        after: 'debug'
      }
    ];
  }
}
