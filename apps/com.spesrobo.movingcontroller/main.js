var spesrobo;

window.onload = function() {
  document.onkeydown = function(e) {
    spesrobo.hardware.moveSpeed(220);

    switch (e.code) {
      case 'KeyUp':
      case 'KeyW':
        spesrobo.hardware.moveForward();
        break;

      case 'KeyDown':
      case 'KeyS':
        spesrobo.hardware.moveBack();
        break;

      case 'KeyLeft':
      case 'KeyA':
        spesrobo.hardware.moveLeft();
        break;

      case 'KeyRight':
      case 'KeyD':
        spesrobo.hardware.moveForward();
        break;
    }
  }

  document.onkeyup = function(e) {
    spesrobo.hardware.moveStop();
  }

  // Because some strange bug in nipple
  window.dispatchEvent(new Event('resize'));

  // Create a new SpesRobo client.
  spesrobo = new SpesRobo.default();
  spesrobo.makeUI();

  spesrobo.hardware.onDataReceived(function(params) {
    console.log(params);
  });
}


var joystick = nipplejs.create({
  zone: document.getElementById('joystick'),
  mode: 'static',
  position: {
    left: '50%',
    top: '50%'
  },
  color: '#2982BE'
});


var jostickLastDirection = null;

joystick.on('move', function(evt, data) {

  var speed = data.distance * 5;
  spesrobo.hardware.moveSpeed(speed);

  if (data.direction != null &&
    data.direction != undefined &&
    jostickLastDirection != data.direction.angle) {

    jostickLastDirection = data.direction.angle;

    switch (data.direction.angle) {
      case 'up':
        spesrobo.hardware.moveForward();
        break;

      case 'down':
        spesrobo.hardware.moveBack();
        break;

      case 'left':
        spesrobo.hardware.moveLeft();
        break;

      case 'right':
        spesrobo.hardware.moveRight();
        break;
    }
  }
});

joystick.on('end', function() {
  spesrobo.hardware.moveStop();
  jostickLastDirection = null;
});



var sliderPosition = new Slider('#slider-position', {
  min: -200,
  max: 200,
  value: 0,
  reversed: true,
  triggerSlideEvent: false,
  triggerChangeEvent: false,
  orientation: 'vertical',
  formatter: function(value) {
    return 'Steps: ' + value;
  }
});

sliderPosition.on('slideStop', function(value) {
  spesrobo.hardware.moveSpeed(255);
  sliderPosition.setValue(0);

  if (value < 0) {
    spesrobo.hardware.moveBack(-value);
  } else {
    spesrobo.hardware.moveForward(value);
  }
});
