var spesrobo;

function setProgressValue(val) {
  val = Math.floor(val * 2.2);
  $('.progress-bar').html(val + '%');
  $('.progress-bar').css('width', val + '%').attr('aria-valuenow', val);
}

function calibrate() {
  spesrobo.hardware.setCommand('ph');
  setTimeout(function(){
    spesrobo.hardware.setCommand('pl');
  }, 500);
}

function requestData() {
  spesrobo.hardware.setCommand('pr');
}

$(document).ready(function() {
  spesrobo = new SpesRobo.default();
  spesrobo.createComponentClientManager();
  spesrobo.createComponentMotion(document.getElementById('joystick'));

  spesrobo.onready = function() {
    setTimeout(function(){
      calibrate();
      requestData();
    }, 500);
  }

  spesrobo.hardware.onDataReceived(function(params) {
    console.log(params);
    if (params[0] == 'Plugin') {
      setProgressValue(params[2]);

      setTimeout(function(){
         requestData();
      }, 1000);
    }
  });
});
