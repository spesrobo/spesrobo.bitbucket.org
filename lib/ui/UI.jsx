import React from 'react';
import ReactDOM from 'react-dom';
import Cookie from './../utils/Cookie.jsx';
import TestingCage from './TestingCage.jsx';
import './UI.css';

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.submitOnClick = this.submitOnClick.bind(this);
  }

  componentDidMount() {
    this.spesrobo = this.props.spesrobo;
    this.setState(this.spesrobo.readyState());

    // Events
    if (this.spesrobo.readyState() != 5) {
      this.makeEvents();
    }
  }

  makeEvents() {
    var that = this;

    this.spesrobo.onerror(function() {
      that.setState(4);
    });
    this.spesrobo.onclose(function() {
      that.setState(that.spesrobo.readyState());
    });
    this.spesrobo.onopen(function() {
      that.setState(that.spesrobo.readyState());
    });
    this.spesrobo.onready(function() {
      that.setState(that.spesrobo.readyState());
    });
  }

  setState(state) {
    var stateNode = ReactDOM.findDOMNode(this.refs.state);
    var backgroundColor = '#ccc';

    switch (state) {
      case 0:
        stateNode.innerHTML = 'Connecting...';
        break;
      case 1:
        stateNode.innerHTML = 'Open';
        break;
      case 2:
        stateNode.innerHTML = 'Closing...';
        break;
      case 3:
        stateNode.innerHTML = 'Closed';
        alert('Connection is closed, please check you connection');
        break;
      case 4:
        stateNode.innerHTML = 'Error';
        break;
      case 5:
        stateNode.innerHTML = 'Not connected';
        break;
    }
  }

  getDefaultIP() {
    if (Cookie.get('address').length > 0) {
      return Cookie.get('address');
    }
    return 'ws://127.0.0.1:1017';
  }

  submitOnClick() {
    var address = ReactDOM.findDOMNode(this.refs.address).value;

    this.spesrobo.setAddress(address);
    Cookie.set('address', address, 100);
    this.makeEvents();
  }

  render() {
    return (
      <div className="holder">
        <div className="container">
          <div className="row">

            {/* Logo */}
            <div className="col-md-2 logo-holder">
              <a href="/store">
                <div className="logo"></div>
                back to store
              </a>
            </div>

            {/* Client managment */}
            <div className="col-md-4 client-holder">
              <div ref="state"></div>
              <div className="form-inline">
                <div className="form-group">
                  <input className="form-control" type="text" ref="address" defaultValue={this.getDefaultIP()}/>
                </div>
                <button className="btn btn-primary" onClick={this.submitOnClick}>Connect</button>
              </div>
            </div>

            {/* Testing Cage */}
            <div className="col-md-4">
              <TestingCage />
            </div>

          </div>
        </div>
      </div>
    );
  }
}
