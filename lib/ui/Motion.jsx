import React from 'react';
import ReactDOM from 'react-dom';
import nipplejs from 'nipplejs';

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.joystick = null;
    this.jostickLastDirection = null;
    this.spesrobo = null;
  }

  componentDidMount() {
    var that = this;

    // Get SpesRobo instance
    this.spesrobo = this.props.spesrobo;

    // Create Joystick UI
    this.joystick = nipplejs.create({
      zone: ReactDOM.findDOMNode(this.refs.joystick),
      mode: 'static',
      position: {
        left: '50%',
        top: '50%'
      },
      color: '#2982BE'
    });

    // Set Joystick actions
    this.joystick.on('move', function(evt, data) {
      var speed = data.distance * 5;
      that.spesrobo.hardware.moveSpeed(speed);

      if (data.direction != null && data.direction != undefined && that.jostickLastDirection != data.direction.angle) {

        that.jostickLastDirection = data.direction.angle;

        switch (data.direction.angle) {
          case 'up':
            that.spesrobo.hardware.moveForward();
            break;

          case 'down':
            that.spesrobo.hardware.moveBack();
            break;

          case 'left':
            that.spesrobo.hardware.moveLeft();
            break;

          case 'right':
            that.spesrobo.hardware.moveRight();
            break;
        }
      }
    });

    document.onkeydown = function(e) {
      that.spesrobo.hardware.moveSpeed(220);
      switch (e.code) {
        case 'KeyUp':
        case 'KeyW':
          that.spesrobo.hardware.moveForward();
          break;

        case 'KeyDown':
        case 'KeyS':
          that.spesrobo.hardware.moveBack();
          break;

        case 'KeyLeft':
        case 'KeyA':
          that.spesrobo.hardware.moveLeft();
          break;

        case 'KeyRight':
        case 'KeyD':
          that.spesrobo.hardware.moveForward();
          break;
      }
    }

    document.onkeyup = function(e) {
      that.spesrobo.hardware.moveStop();
    }

    this.joystick.on('end', function() {
      that.spesrobo.hardware.moveStop();
      that.jostickLastDirection = null;
    });
  }

  render() {
    return (
      <div>
        <div ref="joystick" style={{
          height: '250px',
          width: '250px'
        }}></div>
      </div>
    );
  }
}
