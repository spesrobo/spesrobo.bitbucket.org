import React from 'react';
import './UI.css';

export default class extends React.Component {
  constructor() {
    super();

    this.autoRefresh = false;

    this.state = {
      url: this.getCameraUrl()
    };
    this.reloadImage();
  }

  reloadImage() {
    var self = this;

    if (this.autoRefresh == true) {
      setTimeout(function() {
        self.state.url = self.getCameraUrl();
        self.setState(self.state);
      }, 100);
    }
  }

  getCameraUrl() {
    if (this.autoRefresh == true) {
      return 'http://192.168.1.105:8080/shot.jpg?rnd=' + Math.random();
    } else {
      return 'http://192.168.1.105:8080/video';
      return 'http://192.168.1.110:8081/video.mjpg';
    }
  }

  render() {
    return (
      <div className="tk-holder">
        <img className="tk-image-thumb" src={this.state.url}/>
      </div>
    );
  }
}
