import Hardware from './Hardware.jsx';
import Test from './plugins/Test.jsx';
import FrontCamera from './plugins/FrontCamera.jsx';
import KinectCamera from './plugins/KinectCamera.jsx';
import ColoredObjectDetector from './plugins/ColoredObjectDetector.jsx';
import RobotArm from './RobotArm.jsx';
import Cognitive from './plugins/Cognitive.jsx';

// UI
import React from 'react';
import ReactDOM from 'react-dom';
import UI from './ui/UI.jsx';
import MotionUI from './ui/Motion.jsx';

/**
 * SpesRobo JavaScript library provides whole API for controlling and programming SpesRobo platform remotelly.
 * To use as a standard JavaScript library include `/dist/spesrobo.lib.js` and create object with `var sr = new SpesRobo.default(address)`.
 */
export default class {

  /**
	 * Constructs an instance of SpesRobo.
	 *
	 * @param {string} address - SpesRobo hardware IP address
	 */
  constructor(address) {
    let that = this;

    // Init websocket
    if (address != null && address != undefined) {
      this.setAddress(address);
    }

    // Init internal callbacks
    this.maybeReady = [];

    // Init callbacks
    this.callbacks = [];

    this.hardware = new Hardware(this);

    // Make API public
    // IMPORTANT: Don't use, will be deprecated!
    this.Plugins = {
      Test: Test,
      FrontCamera: FrontCamera,
      ColoredObjectDetector: ColoredObjectDetector,
      KinectCamera: KinectCamera
    };
    this.Test = Test;
    this.RobotArm = RobotArm;
  }


  // Plugins
  createFrontCameraInstance() { return (new FrontCamera(this));  }
  createColoredObjectDetectorInstance() { return (new ColoredObjectDetector(this)); }
  createKinectCameraInstance() { return (new KinectCamera(this)); }
  createRobotArmInstance() { return (new RobotArm(this)); }
  createCognitiveInstance() { return (new Cognitive()); }


  /**
	 * Register callback of a remote method
	 *
	 * @param {string} _object - Name of the object with namespace
	 * @param {string} _method - Name of the method
	 * @param {function} _callback - Function to be called when remote return is ready
	 */
  registerCallback(_object, _method, _callback) {
    this.callbacks[_object + '->' + _method] = _callback;
  }

  /**
	 * Send request for remote method call
	 *
	 * @param {string} _object - Name of the object with namespace
	 * @param {string} _method - Name of the method
	 * @param {array} _parameters - Paramters of the function
	 */
  send(_object, _method, _parameters) {
    let obj = {
      o: _object, // Object
      m: _method, // Method to be called
      p: _parameters // Parameters, [param1, param2, param3...]
    };

    if (this.ws != undefined) {
      this.ws.send(JSON.stringify(obj));
    }
  }

  /**
	 * Event which fire when SpesRobo platform is ready.
	 *
	 * @param {function} callback - Callback Function
	 */
  onready(callback) {
    if (this.ws != null && this.ws != undefined) {
      this.ws.addEventListener('open', callback);
    } else {
      this.maybeReady.push({func: callback, param: []});
    }
  }

  /**
   * Makes SpesRobo Motion Component
   *
   * @param {DOMElement} _element - DOM Element where component will be shown
   */
  createComponentMotion(_element) {
    ReactDOM.render(
      <div>
      <MotionUI spesrobo={this}/>
    </div>, _element);
  }

  /**
	 * Makes SpesRobo Client Managment UI inside an element.
   * It shows connection state, address dialog and managment...
	 *
	 * @param {DOMElement} _element - DOM Element, if undefined it will render in top of the body
	 */
   createComponentClientManager(_element) {
     if (_element != null && _element != undefined) {
       this.element = _element;
     } else {
       this.element = document.createElement('div');
       document.body.insertBefore(this.element, document.body.firstChild);
     }

     // TODO: React should be deleted from SpesRobo JS Core lib (to keep simple and lightweight)
     ReactDOM.render(
       <div>
       <UI spesrobo={this}/>
     </div>, this.element);
   }

  // IMPORTANT: Don't use, will be deprecated!
  makeUI(_element) {
    this.createComponentClientManager(_element);
  }



  readyState() {
    if (this.ws != null) {
      return this.ws.readyState;
    }

    return 5;
  }

  onerror(callback) {
    this.ws.addEventListener('error', callback);
  }

  onclose(callback) {
    this.ws.addEventListener('close', callback);
  }

  onopen(callback) {
    this.ws.addEventListener('open', callback);
  }

  handleCallback(data) {
    let _callback = this.callbacks[data.o + '->' + data.m];
    if (typeof _callback != 'undefined') {
      _callback(data.r);
    }
  }

  setAddress(address) {
    var that = this;

    // Initialize connection
    this.ws = new WebSocket(address);
    this.ws.onmessage = function(object) {
      that.handleCallback(JSON.parse(object.data));
    }

    // Call maybeReady callbacks
    this.maybeReady.forEach(function(obj) {
      that.ws.onopen = function() {
        obj.func(...obj.param);
      }
    });
  }
}
