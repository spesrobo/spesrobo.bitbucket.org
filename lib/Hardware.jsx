export default class {
  constructor(spesrobo) {
    this.spesrobo = spesrobo;

    this.kinectVerticalAngleLastSet = 0;
    this.lastCommandSpeedTime = 0;

    this.THROW_KINECT_VERTICAL_ANGLE_TIME = 1000;
    this.THROW_COMMAND_SPEED_TIME = 100;
  }

  onDataReceived(callback) {
    this.spesrobo.registerCallback('Hardware', 'DataReceived', function(data) {
      var params = data.split('::');
      callback(params);
    });
  }

  setArmCoordinates(x, y, z) {
    this.spesrobo.send('Hardware', 'setArmCoordinates', [x, y, z]);
  }

  setCommand(command) {
    this.spesrobo.send('Hardware', 'setCommand', [command]);
  }

  setKinectAngleVertical(angle) {
    var date = new Date();
    if (this.kinectVerticalAngleLastSet + this.THROW_KINECT_VERTICAL_ANGLE_TIME < date.getTime()) {
      this.kinectVerticalAngleLastSet = date.getTime();
      this.spesrobo.send('Hardware', 'setKinectAngle', [angle]);
    }
  }

  setKinectAngleHorizontal(angle) {
    this.spesrobo.send('Hardware', 'setCommand', ['ka' + angle]);
  }

  moveLeft() {
    this.spesrobo.send('Hardware', 'setCommand', ['ml']);
  }

  moveRight() {
    this.spesrobo.send('Hardware', 'setCommand', ['mr']);
  }

  moveForward(steps) {
    var s = (steps != undefined && steps != null)
      ? parseInt(steps)
      : '';
    this.spesrobo.send('Hardware', 'setCommand', ['mf' + s]);
  }

  moveBack(steps) {
    var s = (steps != undefined && steps != null)
      ? parseInt(steps)
      : '';
    this.spesrobo.send('Hardware', 'setCommand', ['mb' + s]);
  }

  moveStop() {
    this.spesrobo.send('Hardware', 'setCommand', ['mo']);
  }
  moveSpeed(speed) {
    // Not so important, let's reduce number of the errors
    var date = new Date();
    if (this.lastCommandSpeedTime + this.THROW_COMMAND_SPEED_TIME < date.getTime()) {
      this.lastCommandSpeedTime = date.getTime();
      this.spesrobo.send('Hardware', 'setCommand', ['mv' + parseInt(speed)]);
    }
  }
}
