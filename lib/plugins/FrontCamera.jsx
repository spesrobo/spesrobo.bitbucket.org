import Helper from '../Helper.jsx';
import Converter from '../utils/Converter.jsx';

export default class FrontCamera {
  constructor(spesrobo) {
    this.spesrobo = spesrobo;
    this.id = Helper.generateObjectID();
    global.spesrobo.send('SpesRobo.Plugins.FrontCamera', 'new', [this.id]);
  }

  getFrame(callback) {
    this.spesrobo.send(this.id, 'getFrame', []);
    this.spesrobo.registerCallback(this.id, 'getFrame', callback);
  }

  getBinaryFrame(data) {
    return Converter.getBinary(data);
  }
}
