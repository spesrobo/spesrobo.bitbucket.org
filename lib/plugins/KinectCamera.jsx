import Helper from '../Helper.jsx';


export default class KinectCamera {
  constructor(spesrobo) {
    this.spesrobo = spesrobo;
    this.id = Helper.generateObjectID();
    global.spesrobo.send('SpesRobo.Plugins.KinectCamera', 'new', [this.id]);
  }

  getFrame(callback) {
    this.spesrobo.send(this.id, 'getFrame', []);
    this.spesrobo.registerCallback(this.id, 'getFrame', callback);
  }
}
