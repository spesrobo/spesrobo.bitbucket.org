import Helper from '../Helper.jsx';

export default class ColoredObjectDetector {
  constructor(spesrobo) {
    this.spesrobo = spesrobo;
    this.id = Helper.generateObjectID();
    this.spesrobo.send('SpesRobo.Plugins.ColoredObjectDetector', 'new', [this.id]);
  }

  detectObject(callback) {
    this.spesrobo.send(this.id, 'DetectObject', []);
    this.spesrobo.registerCallback(this.id, 'DetectObject', callback);
  }

  getObjectX(callback) {
    this.spesrobo.send(this.id, 'GetObjectX', []);
    this.spesrobo.registerCallback(this.id, 'GetObjectX', callback);
  }

  getObjectY(callback) {
    this.spesrobo.send(this.id, 'GetObjectY', []);
    this.spesrobo.registerCallback(this.id, 'GetObjectY', callback);
  }

  getObjectZ(callback) {
    this.spesrobo.send(this.id, 'GetObjectZ', []);
    this.spesrobo.registerCallback(this.id, 'GetObjectZ', callback);
  }
  changeColor(r, g, b) {
    this.spesrobo.send(this.id, 'ChangeColor', [r, g, b]);
  }
  changeAreaSize(size)
  {
    this.spesrobo.send(this.id, 'ChangeAreaSize', [size]);
  }
}
