import Converter from '../utils/Converter.jsx';

export default class {
  constructor() {
    this.EMOTION_KEY = '';
    this.EMOTION_URL = 'https://api.projectoxford.ai/emotion/v1.0/recognize';
  }

  analyzeEmotion(image, callback) {
    if (this.EMOTION_KEY.length == 0) {
      console.warn('Please set EMOTION_KEY');
      return null;
    }

    var binaryData = Converter.getBinary(image);
    this.sendBinary(binaryData, this.EMOTION_URL, this.EMOTION_KEY, callback);
  }

  sendBinary(data, url, key, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Ocp-Apim-Subscription-Key', key);
    xhr.setRequestHeader('Content-Type', 'application/octet-stream');

    xhr.send(data.buffer);

    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && xhr.status == 200) {
        callback(JSON.parse(xhr.response));
      }
    }
  }
}
