import Helper from '../Helper.jsx';

export default class Test {
  constructor(spesrobo) {
    this.spesrobo = spesrobo;
    this.id = Helper.generateObjectID();
    global.spesrobo.send('SpesRobo.Plugins.Test', 'new', [this.id]);
  }

  sampleMethod(param, callback) {
    this.spesrobo.send(this.id, 'sampleMethod', [param]);
    this.spesrobo.registerCallback(this.id, 'sampleMethod', callback);
  }

  onSampleEvent(callback) {
    this.spesrobo.registerCallback(this.id, 'sampleEvent', callback);
  }
}
