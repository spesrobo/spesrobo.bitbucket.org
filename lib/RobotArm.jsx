export default class RobotArm {
  constructor(spesrobo)
  {
    this.spesrobo = spesrobo;
  }

  setAngle(crank, angle)
  {
    switch (crank) {
      case 0:
        if (angle >= 30 && angle <= 100)
          this.spesrobo.hardware.setCommand("af" + Math.floor(angle));
        break;
      case 1:
        if (angle >= 65 && angle <= 130)
          this.spesrobo.hardware.setCommand("as" + Math.floor(angle));
        break;
      case 3:
        if (angle >= 120 && angle <= 250)
          this.spesrobo.hardware.setCommand("at" + Math.floor(angle));
        break;
    }
  }

  isCoordinateReachable(x, y, z, deepCheck) {

    if (Math.sqrt(y * y + z * z) > 182 + 163) {
      return false;
    }
    if (182 > 163 && Math.sqrt(y * y + z * z) < 182 - 163) {
      return false;
    }

    /*if (deepCheck == true) {
		var coords = {p:0,m:0,n:0};
		this.getCoordinateOfSecondCrank(x, y, z, coords);
		if (coords.m != coords.m || coords.n != coords.n) {
			return false;
		}
	}*/

    return true;
  }

  setArmCoordinates(x, y, z) {

    if (this.isCoordinateReachable(x, y, z, true) == false) {
      return;
    }
    // Cordinates of Second crank
    var coords = {
      p: 0,
      m: 0,
      n: 0
    };
    this.getCoordinateOfSecondCrank(x, y, z, coords);

    // Set angles
    var alpha = Math.atan2(coords.n, coords.m) * 180 / Math.PI;
    if (alpha < 30 || alpha > 100)
      return;
    this.setAngle(0, alpha);

    var beta = Math.atan2(y - coords.n, coords.p - coords.m) * 180 / Math.PI + 180 - alpha;
    if (beta < 65 || beta > 130)
      return;
    this.setAngle(1, beta);

    var gama = Math.atan2(z, x) * 180 / Math.PI;
    this.setAngle(3, gama);

    console.log(alpha + " " + beta + " " + gama);

  }

  getCoordinateOfSecondCrank(x, y, z, coords) {

    // Calculate angle for First and Second servo
    var l = 182;
    var d = 163;

    coords.p = Math.sqrt(x * x + z * z);
    // Huh, some crazy Math :(
    // (m, n) are coordinates of Second crank
    // a = z, b = y
    // http://www.wolframalpha.com/input/?i=m%5E2%2Bn%5E2%3Dl1%2C+%28m-a%29%5E2%2B%28n-b%29%5E2%3Dl2
    coords.m = (1.0 / (2 * (z * z + y * y))) * ((-d) * d * z - Math.sqrt((-y) * y * (d * d * d * d - 2 * d * d * l * l - 2 * d * d * z * z - 2 * d * d * y * y + l * l * l * l - 2 * l * l * z * z - 2 * l * l * y * y + z * z * z * z + 2 * z * z * y * y + y * y * y * y)) + l * l * z + z * z * z + z * y * y);
    coords.n = (1.0 / (2 * y * (z * z + y * y))) * (-d * d * y * y + z * Math.sqrt(-y * y * (d * d * d * d - 2 * d * d * l * l - 2 * d * d * z * z - 2 * d * d * y * y + l * l * l * l - 2 * l * l * z * z - 2 * l * l * y * y + z * z * z * z + 2 * z * z * y * y + y * y * y * y)) + 1.0 * l * l * y * y + z * z * y * y + y * y * y * y);
  }
}
