module.exports = {
  entry: __dirname + "/SpesRobo.jsx",
  output: {
    path: __dirname + '/dist',
    filename: 'spesrobo.lib.js'
  },
      devtool: 'source-map',
  module: {
    loaders: [
      //{ test: /\.js$/, loader: 'uglify' },
		  {
			  test: /\SpesRobo.jsx?$/,
			  loader: 'expose?SpesRobo',
		  },
      {
        test: /\.png?$/,
        loader: "url?prefix=image/&limit=100000"
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015']
        }
      }, {
        test: /\.css$/,
        loader: "style!css!"
      }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file"
      }, {
        test: /\.(woff|woff2)$/,
        loader: "url?prefix=font/&limit=5000"
      }, {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=application/octet-stream"
      }, {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=image/svg+xml"
      },
    ]
  }
};
