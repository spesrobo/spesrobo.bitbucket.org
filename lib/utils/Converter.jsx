export default class {
  /**
   * Converts base64 image to binary
   * Reference: http://stackoverflow.com/questions/32113907/how-convert-image-into-binary-format-using-javascript
   *
   * @param {String} - Image encoded to base64
   * @return {Binary} - Binary image
   */
  static getBinary(data) {
    var BASE64_MARKER = ';base64,';
    var base64Index = data.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    var base64 = data.substring(base64Index);
    var raw = window.atob(base64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));

    for(let i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;


    /*
    var data = image.replace(/data\:image\/jpg;base64,/, '');

    var binArray = []
    var datEncode = '';

    for (let i = 0; i < data.length; i++) {
      binArray.push(data[i].charCodeAt(0).toString(2));
    }

    for (let j = 0; j < binArray.length; j++) {
      var pad = padding_left(binArray[j], '0', 8);
      datEncode += pad + ' ';
    }

    function padding_left(s, c, n) {
      if (!s || !c || s.length >= n) {
        return s;
      }
      var max = (n - s.length) / c.length;
      for (var i = 0; i < max; i++) {
        s = c + s;
      }
      return s;
    }

    return binArray;
    */
  }


  // TODO
  static getImageBinary(data) {
    //var base64 = data.replace(/data\:image\/jpg;base64,/, '');
    //return this.getBinary(base64);
  }
}
